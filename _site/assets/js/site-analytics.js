var $ctaButton1 = $('#cta-button-1');
var $contactFormButton = $('#contact-form-button');

$ctaButton1.on('click', function() {
  gtag('event', 'Click', {
    'event_category': 'Buttons',
    'event_label': 'TokenCarta Landing Page 1 - 12.18.2017 - CTA 1'
  });

  analytics.track('Clicked CTA 1 Button', {
    landingPage: 'TokenCarta Landing Page 1 - 12.18.2017',
  });
});

$contactFormButton.on('click', function() {
  var email = $('#email-address').val();

  if (email !== "") {
    var uuid = generateUUIDv4();

    analytics.identify(uuid, {
      email: email
    });
  } else {
    return;
  }

  gtag('event', 'Click', {
    'event_category': 'Buttons',
    'event_label': 'TokenCarta Landing Page 1 - 12.18.2017 - Contact Form Button'
  });

  fbq('track', 'TokenCarta Contact Button');

  analytics.track('Clicked Contact Form Button', {
    landingPage: 'TokenCarta Landing Page 1 - 12.18.2017',
  });
});

function generateUUIDv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

