#!/bin/bash
#
# Build assets and .html pages, move into _site folder, and deploy site.
#
# Usage:
#   ./deploy.sh

# Delete all previous build files inside _site directory
rm -rf _site

# Run gulp to build sass files
gulp build

# Run jekyll to build blog
( cd blog && jekyll build JEKYLL_ENV=production )

# Move files over to the _site deploy directory
cp -a src/ _site/

# Move jekyll blog over to the _site deploy directory
cp -a blog/_site/* _site/

# Move static downloadable files over to _site deploy directory
cp -a files/ _site/files

# Create a commit and deploy to netlify
now=$(date)
git add -A
git commit --allow-empty -m "Deploying @ ${now}"
git push origin master

