---
layout: post
title:  "How To Get 16 HVAC Leads In The Next 28 Days"
description: "Learn how a small HVAC company in Florida got more leads with digital marketing. Apply the same strategies they used. The ultimate guide to growing your HVAC business."
thumbnail_image: "how-to-get-16-hvac-leads-in-the-next-28-days-hero.jpg"
---

# How To Get 16 HVAC Leads In The Next 28 Days

{% asset how-to-get-16-hvac-leads-in-the-next-28-days-hero.jpg alt="How To Get 16 HVAC Leads In The Next 28 Days Hero Image" class="hero" %}

It’s difficult getting people to contact your business to request your HVAC services.

It’s even harder to do it on a consistent basis, day to day.

All HVAC companies have a different approach on how they generate their leads.

Advertising in newspapers and phone books, buying leads from HomeAdvisor and Angie’s List, mailing flyers and postcards, word of mouth, online directories.

There are numerous ways to get your business’s name out there in hopes of finding your next customer.

**Surprisingly, there is one particular channel to get leads that many companies overlook.**

A channel where people are interested specifically in your HVAC services and are looking to get a quote or set up a consultation ASAP.

{% include hvac-checklist-offer.html %}

In this article, I’ll show you how one HVAC company used that channel to generate 16 HVAC leads in 28 days.

**After reading this article, you’ll also be able to implement the same strategy to get the leads rolling in.**

Let’s get started.

## How an HVAC company in Florida Generated 16 Leads in 28 Days

This small HVAC company in Florida was doing all the usual things that businesses do to drum up sales.

They were buying ad space in newspapers and phone books…

...But who uses newspapers to look for professional services? When was the last time someone opened a phone book for anything?

{% asset phonebooks.jpg alt="Phone books" %}
<small>Don't let your ad dollars get thrown in here.</small>

They were buying leads from HomeAdvisor, Thumbtack, and Angie’s List sales reps…

...The same leads which are sold to tens of other companies which results in a race to who can offer their services the cheapest.

**Suffice to say, this HVAC company was struggling to get quality leads.**

They rethought their approach and found out that most people today search on the internet to find professional service providers in their area.

A decision was made to spend all of their monthly advertising budget purely on digital marketing and online advertising.

This proved to be the right call. One that increased the number and quality of leads they were getting.

**The online advertising channel that got them the best results was Google AdWords.**

Now, some of you may have already used Google AdWords and have had a less than stellar experience.

Many companies spend thousands of dollars on it and never see much of a result.

Unfortunately, this is very common. Many businesses end up losing a ton of money when using AdWords.

Google sales reps and shady “professional marketing consultants" will lead you to believe it is as easy as creating a text ad and targeting a few keywords.

It’s not. The only thing that will do is burn through your money with nothing to show for it.

**AdWords campaigns need to be set up correctly with the optimal settings and options for it to work well.**

Fortunately, this HVAC company learned how to use AdWords the right way.

They were able to get 16 leads in 28 days for $37.03 per lead. The total cost of their campaign for the month was $592.56.

<a href="{% asset hvac-ppc-campaign.png @path %}" target="_blank">{% asset hvac-ppc-campaign.png alt="Screenshot of Florida HVAC Company's PPC Campaign" %}</a>
<small>A very successful campaign. Click to enlarge image.</small>

**These are warm, targeted leads. People looking specifically for their HVAC services. Leads that are 100% theirs and not resold to tens of other businesses.**

Sounds pretty good, huh?

I’ll walk you through step by step exactly how they did it.

Follow the same steps and you’ll also be able to create an effective AdWords campaign for your company.

## Set Your Service Locations

The HVAC company is based in Naples, Florida. As most service based businesses, they only take on work in their surrounding area.

They set up their ads so that they are only shown to people in the specific cities and counties they serve.

<a href="{% asset service-locations.jpg @path %}" target="_blank">{% asset service-locations.jpg alt="Screenshot of PPC Target Locations" %}</a>
<small>Make sure to only target the areas you service. Click to enlarge image.</small>

By doing this, they aren’t wasting money showing their ads to people on the other side of the country.

Not targeting the specific locations you service will cost your business serious money. Your ad spend will be through the roof and you’ll have worthless leads.

**It’s important to only select the specific cities, towns, and counties that you service. Your ads will only appear to people in these areas.**

AdWords lets you choose from specific towns, zip codes, counties, and even setting a certain mile radius around a location.

## Write Attention Grabbing Text Ads

Writing text ads that grab the attention of your potential customer is an art.

Especially when you have a limited amount of characters to do it.

**A good ad needs to have a strong message and tells the person that you are the right company to solve their problem. The ad needs to stand out.**

The best way to do that is to look at it from your customer’s perspective. What are they looking for in an HVAC company?

**What benefits can you highlight and provide that makes your company stand out from the others?**

Here are some of the text ads I get when searching for a commercial HVAC service provider in my area:

<a href="{% asset list-of-benefits-on-adwords-text-ad.png @path %}" target="_blank">{% asset list-of-benefits-on-adwords-text-ad.png alt="Screenshot of HVAC Google Ads" %}</a>
<small>Talk about how you can help your customer. Make it about them. Click to enlarge image.</small>

Which one would you click on?

**An effective ad should always focus on how a business can help their customer.**

Write about what makes your business different from the rest. Write about why you are the best choice.

A headline that includes your location and your offer of a free quote works very well. This is the headline that the HVAC company in Naples used to great effect: “**Naples #1 Commercial HVAC Installer \| Get Your Free Quote**”.

Mention how many years of experience you have. Mention how many service jobs you have completed.

Mention that you are certified. Mention your BBB rating. Mention the many positive reviews and testimonials you have.

If you offer free quotes, consultations, coupons, and financing options, don’t forget to mention that as well.

**All of the above will get your potential customer to contact you instead of your competitors.**

Do your own Google search in your area and see what the other HVAC ads look like.

Most likely, they are just listing the services they offer.

Write a compelling ad that focuses on your customer’s concerns. That is what will grab their attention and bring them to your landing page.

## Use Ad Extensions

Ad extensions are a way to expand your ad to include more information about your business.

Too often, I see ads not taking advantage of extensions.

They are a great way to make your ad bigger, take up more screen “real estate” for visibility, and make your business stand out from the others.

There are many different types of ad extensions. I’ll list the ones every HVAC company should be using for their ads.

### Callout Extensions

Callouts are short 12-15 character bullet points you can use to highlight certain aspects of your business.

<a href="{% asset callout-extensions.png @path %}" target="_blank">{% asset callout-extensions.png alt="Callout Extensions on HVAC Google Ad" %}</a>

They are a great way to highlight how your company is different from your competitors.

As I mentioned before on how to write great text ads, this is where you want to spend a lot of your attention on how you differ from your competitors.

91 Digital did great research on [effective callout extensions](https://91digital.net/blog/list-of-callout-extension-examples/){:target="_blank"} you can use. I’ll list some of the ones that have worked well for many HVAC businesses:

* Licensed HVAC Experts
* Licensed and Insured
* Trusted for x Years
* Free Estimates
* Free Quotes
* Local Service
* Fast Response
* Affordable Pricing
* Financing Available
* HVAC Specialists
* x Years Experience
* x+ Companies Serviced
* x+ Homes Serviced
* Quick Turnaround
* x Year Guarantee
* x Year Wanrranty

### Location Extensions

Location extensions are another important part of your ad. They let your potential customer know that you are an established business in the area, and not a fly-by-night operation.

<a href="{% asset location-extension.png @path %}" target="_blank">{% asset location-extension.png alt="Location Extension on HVAC Google Ad" %}</a>

Creating trust is an important part of getting leads. Using your business location helps to build that trust.

Plus, it also helps your ad take up more space on the screen.

### Call Extensions

Having your phone number listed on your ad is essential.  Some people prefer to call in and speak on the phone to ask questions.

Putting your phone number in your ad makes it easy for these people to quickly contact you.

<a href="{% asset call-extension.png @path %}" target="_blank">{% asset call-extension.png alt="Call Extension on HVAC Google Ad" %}</a>

<a href="{% asset call-extension-mobile.png @path %}" target="_blank">{% asset call-extension-mobile.png alt="Mobile Call Extension on HVAC Google Ad" %}</a>

Call extensions make it simple for your potential customer to get in touch with you almost instantaneously.

If the person who sees the ad is on their phone, they can just tap the number to call.

Again, it also helps your ad take up more screen space which helps to capture your customer’s attention.

## Choose Your Target Keywords

Choosing the right keywords to target is important. You want to target keywords that have “buyer’s intent”. I’ll explain.

Let’s say you target the keyword “air conditioner”. This keyword is very broad and can match a variety of topics such as “air conditioner covers”, “the best air conditioner”, “window air conditioners”, or “air conditioner brands”.

These are keywords that do not relate to your business. You do not want your ads to show up when people search for these unrelated topics.

**You’ll be wasting money showing ads to these people who have no interest in your business.**

A “buyer intent” keyword is a search term used by people actually looking for your services.

Here is a list of buyer intent keywords you can use for your HVAC services:

* central ac installation
* ductless ac installation
* ac installation companies
* ac installation contractors
* emergency ac repair
* home ac repair
* ac repair services
* commercial hvac companies
* commercial hvac contractors
* commercial hvac repair
* commercial hvac installation
* commercial hvac service
* commercial hvac maintenance
* commercial hvac (your city)
* commercial hvac (your state)
* (your city) commercial hvac
* (your state) commercial hvac

## Avoid Negative Keywords

As I mentioned in the previous section, you want to only target “buyer intent” keywords and avoid search terms that have nothing to do with your business.

For example, if someone searches “ac installation” you don’t want your ad to appear for someone searching for “ac installation parts”.

**Setting up negative keywords allows you to do this. Negative keywords makes it so your ad does not show up for search terms that include certain words.**

Here is a list of negative keywords you can use that are related to the HVAC industry:

* covers
* brands
* sales
* parts
* diy
* leads
* jobs
* unit
* cost
* permit
* salary
* resume
* tools

With everything set up as mentioned by the steps above, you now have an AdWords campaign ready to target people who are looking specifically for your services.

The next step is to make sure the potential customers clicking on your ads go to your website and can easily contact you.

## Create The Perfect Landing Page

The biggest mistake most HVAC businesses make is sending visitors who click on their ads to their website’s home page.

This part may confuse some of you.

Everyone is told their business website needs to be packed full of information to educate their visitor.

Content about how a particular service is completed. What an installation entails. Pages and pages with information about the business.

**You actually have to do the opposite when leading a visitor to your website from your ad.**

You have to condense the information on your website into one page and focus on getting your potential customer to contact you.

Now, that’s not to say your website isn’t working or needs to be remade.

Your website is fine. What I’m talking about is something called a landing page.

**A landing page is a web page that has only the most relevant and important information your customer needs to make the decision to contact you.**

**The sole purpose of a landing page is to capture your visitor’s contact details.**

Landing pages are very concise, short, and to the point. It has no links to other pages. It offers no distractions.

It’s only focus is to get your visitor to contact you. It has only the most essential information.

Here is the landing page that the HVAC company in Florida used to generate their 16 leads in 28 days:

<a href="{% asset all-zone-landing-page.jpg @path %}" target="_blank">{% asset all-zone-landing-page.jpg alt="Screenshot of All Zone Air Conditioning Landing Page" %}</a>
<small>A very simple landing page that converts extremely well and gets leads like a magnet. Click to enlarge.</small>

When a potential customer clicks on the ad, they are sent to this page.

This landing page is completely focused on getting the customer to request their free estimate.

**Landing pages are lead generating machines.**

They are much better than sending your visitors to your website’s home page.

Why?

Your home page has too many actions the visitor can take. They can click on multiple different links and be taken to different pages.

They can get distracted looking at too many photos and reading too much content.

Visitors can easily become overwhelmed and even forget the reason they came to your website.

This happens more often than you think.

**Landing pages eliminate that problem completely.**

Take another look at the page above. It’s obvious what the visitor needs to do.

They should either call your business or fill out the form to request a free estimate.

Simple, straightforward, and to the point.

**Using a landing page can get 15% to 30% (in some cases, even higher) of your visitors to contact you.**

A much higher percentage than by just sending those visitors to your home page.

This is the essential information you need to include on your landing page, nothing more, nothing less:

* Your company logo.
* Your phone number in big bold text.
* A clear call-to-action that hooks your customer (example: “Get Your FREE Estimate!” or “Contact Us Today For Your FREE Estimate”).
* A couple of relevant photos relating to HVAC. Photos of jobs you’ve completed or a group photo of your team.
* A contact form with only the name, phone number, email, and message fields.
* A few paragraphs talking about your business, what separates you from your competitors, and why your company is the best choice.
* A list of benefits that you provide.
* A list of services that you provide.
* Social proof, testimonials, and quality badges.
* Make sure your landing page is properly formatted for phones. Your page has to look good and be easy to navigate on a mobile device. More than 50% of people now start their service research on their phones. You cannot ignore this!

With the information above, you give your visitor just the right amount of information to get them to inquire about your services.

## Set Your Budget

This is where things will differ the most between HVAC companies.

Perhaps you have just recently started your fledgling HVAC business. Your advertising budget might be small.

Or you may already have an established team with 10+ years of experience and more advertising dollars to burn.

**Either way, you need to figure out how much you’ll be able to spend on your AdWords lead generation campaign.**

AdWords campaigns charge you by the click. You can set either a daily or monthly budget.

If you are new to AdWords, I recommend going with a daily budget so you can test the waters and not spend too much too soon.

To start out, I recommend setting a budget that will get you about 4 clicks per day.

{% asset turtle-racing.jpg alt="Turtle racing" %}
<small>Start small and slow with AdWords, then ramp up ad spend when you begin getting leads and sales.</small>

How much does a click cost? It depends on a couple of factors. The keyword, how many others are bidding on the same keyword, your location, the quality of your ad, and other measures.

I’ve found that the average cost of an service related HVAC "buyer intent" keyword is about $9 nationwide.

Let’s do some math.

**If the average cost-per-click is $9, you should set a daily budget of $36. That will get you 4 clicks. Run your ads only from Monday to Friday. The cost of running your ads for 20 days a month at $36 per day will be a total cost of $720 a month.**

For that $720 spent, you should be able to generate 15 to 30 warm, targeted HVAC leads per month.

**These are very good numbers. A phone book or HomeAdvisor won’t be able to get you quality leads like this.**

The numbers above are similar results the HVAC company in Florida got. With their tight AdWords budget, they started to land regular multi-thousand dollar residential jobs within the month. After that, they grew their budget which led to landing bigger $10,000+ commercial jobs.

Once you start closing more sales and getting bigger jobs, you can then increase your AdWords budget and start getting 50 to 100 or more leads per month.

**The return on investment with a properly set up AdWords campaign and optimized landing page is huge.**

Only the sky’s the limit when you have your own digital marketing funnel set up, working to get you leads like clockwork.

## Wrap Up

I hope this guide has given you a new perspective on how you can use Google AdWords to generate leads for your HVAC business.

**Your customers are all searching for your services online.**

You don’t need to waste money on old school methods like phone books and flyers.

You don’t need to rely on dubious lead vendors and pushy salespeople at HomeAdvisor and Angie’s List.

You no longer need to rely on word of mouth or referrals.

It’s now possible to create your own digital marketing system that gets you a steady stream of HVAC leads without breaking the bank.

**Your own leads created by your own efforts. A pipeline of leads that you control.**

{% asset flowing-pipeline.jpg alt="Flowing pipeline" %}
<small>A well oiled marketing system will have you flooded with leads.</small>

As for the HVAC company in Florida?

They are no longer struggling. They are now growing and thriving.

**If you want to learn more about how to set up your own digital marketing system with AdWords, I can personally walk you through everything and answer any questions you have.**

I’m a software developer and digital marketer that works with HVAC companies around the nation.

Some of the HVAC companies I work with have big budgets and need hundreds of commercial leads per month.

Some companies are small and growing, and they only need 20 residential leads a month.

**I work with HVAC companies and budgets of all sizes. I focus on setting up and running their AdWords campaigns so they can have their own lead generation system in place.**

Again, I only work with HVAC companies. That’s where my specialty lies.

**[Click here to contact me today and schedule your one on one HVAC Lead Generation Blueprint strategy session.](/hvac-lead-generation-blueprint-strategy-session)**

The HVAC Lead Generation Blueprint strategy session is a 30-45 minute call that’s completely focused on your business.

We’ll talk about what marketing efforts have been working for your business. We’ll also talk about what hasn’t worked.

**The call is all about finding out how we can improve your business’s marketing strategy. We’ll figure out how we can make your business grow.**

After the call, I’ll create a custom AdWords plan tailor made specifically for your business.

**You’ll be able to implement this plan to start getting more leads immediately.**

A custom plan like the one the HVAC company in Florida in this guide implemented.

**[Click here to schedule your one on one HVAC Lead Generation Blueprint strategy session.](/hvac-lead-generation-blueprint-strategy-session)**

Thanks for reading this guide. If you found it useful, please like and share it on Facebook, or let your HVAC friends know. Pass the word along, I would really appreciate it.

I wish you the best of luck with your business!

\- Alex

{% include hvac-checklist-offer.html %}
