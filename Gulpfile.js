'use strict';

var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var cssnext = require('postcss-cssnext');
var cssnano = require('cssnano');
var browserSync = require('browser-sync').create();

gulp.task('serve', ['scss'], function() {
  browserSync.init({
    server: {
      baseDir: './',
      directory: true
    }
  });

  gulp.watch('assets/sass/**/*.scss', ['scss']);
  gulp.watch('**/*.html').on('change', browserSync.reload);
});

gulp.task('scss', function() {
  var processors = [
    cssnext,
    // cssnano
  ];

  return gulp.src('assets/sass/**/*.scss')
          .pipe(sass().on('error', sass.logError))
          .pipe(postcss(processors))
          .pipe(gulp.dest('./assets/css/'))
          .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
gulp.task('build', ['scss']);
