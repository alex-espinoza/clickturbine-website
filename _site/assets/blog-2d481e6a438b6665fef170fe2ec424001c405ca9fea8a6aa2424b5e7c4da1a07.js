$(function() {
  $('.open-popup-link').magnificPopup({
    type: 'inline',
    midClick: true,
    focus: '#mce-EMAIL',
    mainClass: 'mfp-fade',
    fixedBgPos: true,
    fixedContentPos: true
  });

  // Exit intent bonus popup
  function addEvent(obj, evt, fn) {
    if (obj.addEventListener) {
      obj.addEventListener(evt, fn, false);
    } else if (obj.attachEvent) {
      obj.attachEvent("on" + evt, fn);
    }
  }

  // Exit intent trigger for bonus popup
  addEvent(document, 'mouseout', function(evt) {
    if (evt.toElement === null && evt.relatedTarget === null && !localStorage.getItem('bonus_popup_show')) {
      $.magnificPopup.open({
        items: {
          src: '#bonusPopup'
        },
        type: 'inline',
        focus: '#mce-EMAIL',
        mainClass: 'mfp-fade',
        fixedBgPos: true,
        fixedContentPos: true
      });

      localStorage.setItem('bonus_popup_show', 'true');
    }
  });

  // Users interested in content bonus popup after scrolling 50% of page
  $(document).scroll(function(e) {
    var scrollAmount = $(window).scrollTop();
    var documentHeight = $(document).height();

    var scrollPercent = (scrollAmount / documentHeight) * 100;

    if (scrollPercent >= 50) {
      showBonusPopupAfterScroll();
      $(document).off('scroll');
    }
  });

  function showBonusPopupAfterScroll() {
    if (!localStorage.getItem('bonus_popup_show')) {
      $.magnificPopup.open({
        items: {
          src: '#bonusPopup'
        },
        type: 'inline',
        focus: '#mce-EMAIL',
        mainClass: 'mfp-fade',
        fixedBgPos: true,
        fixedContentPos: true
      });

      localStorage.setItem('bonus_popup_show', 'true');
    }
  }

  // handle AJAX mailchimp email subscription
  $mailchimpForm = $('#mc-embedded-subscribe-form');
  $mailchimpFormSubcribeButton = $('#mc-embedded-subscribe');
  $mailchimpFormSuccessMessage = $('#mc_success_msg');
  $mailchimpFormErrorMessage = $('#mc_error_msg');

  $mailchimpFormSubcribeButton.on('click', function(event) {
    event.preventDefault();
    mailchimpAjaxSubscribe();
  });

  function mailchimpAjaxSubscribe() {
    $mailchimpFormErrorMessage.hide();
    mailchimpFormSubcribeButtonText = $mailchimpFormSubcribeButton.val();
    $mailchimpFormSubcribeButton.val('Sending checklist...');
    $mailchimpFormSubcribeButton.attr('disabled', 'disabled');

    $.ajax({
      type: $mailchimpForm.attr('method'),
      url: $mailchimpForm.attr('action'),
      data: $mailchimpForm.serialize(),
      cache: false,
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      error: function (error) {
        $mailchimpFormErrorMessage.show();
        $mailchimpFormSubcribeButton.val(mailchimpFormSubcribeButtonText);
        $mailchimpFormSubcribeButton.removeAttr('disabled');
      },
      success: function (data) {
        if (data.result === 'success') {
          $mailchimpForm.hide();
          $mailchimpFormSuccessMessage.show();
        } else {
          $mailchimpFormErrorMessage.show();
          $mailchimpFormSubcribeButton.val(mailchimpFormSubcribeButtonText);
          $mailchimpFormSubcribeButton.removeAttr('disabled');
        }
      }
    })
  }
});
